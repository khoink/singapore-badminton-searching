/**
 * Created by khoinguyen on 2/4/15.
 */


$(document).ready(function() {

    var YQL_ENPOINT = "https://query.yahooapis.com/v1/public/yql?q=";
    var total = 0;
    var count = 0;

    var viewMode = $("#mode").val();
    var tp = $("#tp").val();
    var time = $("#time").val();

    var fulfillLocations = function() {
        status('Fulfilling locations ..');
        $.ajax({
            url: YQL_ENPOINT + encodeURI('SELECT * FROM html WHERE url=\'https://one.pa.' +
                'gov.sg/CRMSPortal/CRMSPortal.portal\' and xpath=\'//select[@id="homeMenu.searchLocation"]/option\'').replace('=', '%3D')
                + '&format=json'
        }).done(function(data) {
            var optionHtml = '';
            $.each(data.query.results.option, function(index, option) {
                if(index != 0) {
                    optionHtml = optionHtml.concat('<option value="' + option.value + '">' + option.content + '</option>');
                }
            });
            $('#locations').html(optionHtml);
            status("Ready!");
        });
    }

    var search = function() {
        time = $("#time").val();
        tp = $("#tp").val();
        $("#result").html('');
        status('Searching for available CC(s)..');
        var date = $('#date').val();
        var viewMode = $('#mode').val();
        var preferTime = $('#time').val();

        var exclusionOptions = $("#exclusions option");


        $("#jqxProgressBar").progressbar({
            value: 1
        });

        var query = encodeURIComponent("select * from htmlpost where url='https://one.pa.gov.sg/CRMSPortal/CRMSPortal.portal?_nfpb=true&_st=" +
            "&_windowLabel=CRMSPortal_1&_urlType=action&wlpCRMSPortal_1_action=RBMFacilityPublicBooking&_pageLabel=CRMSPortal_page_1'");

        var postDataObject = {
            searchDate: date,
            searchResource: 9,
            indSearchBy: 1,
            searchResultPerPage: 500,
            viewMode: 1,
            pageNo: 1,
            "btnSearch.x": 43,
            "btnSearch.y": 8,
            searchLocation: ""
        };
        var postData = objectToQueryParams(postDataObject);

        query = query.concat(encodeURIComponent(" and postdata=\"" + postData + "\""));
        var xpath = " and xpath='//table[@class=\"main_table\"]'";
        var PAGE = 1;

        $.ajax({
            url: YQL_ENPOINT + query + encodeURIComponent(xpath).replace('=', '%3D') + "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys"
        }).done(function(data) {
            var availableCourts = [];
            $.each(data.query.results.postresult.table, function(index, table) {
               if(table.tbody) {
                   availableCourts = table.tbody.tr;
               }
            });
            var results = [];
            if(availableCourts.length > 2) {
                status('Looking for available vacancies..');
                $.each(availableCourts, function(index,court) {
                    if(index >= 2 ) {
                        if(court.td[6] && court.td[6].input.name == 'btnCheckVacancy' && (court.td[5].p == 'Available Online'
                                || court.td[5].content == 'Available Online')) {
                            var courtData = court.td[6].input.onclick;
                            var betterData = courtData.substring(courtData.indexOf("'"), courtData.indexOf(')'));
                            betterData = betterData.replace(/\'/g, '').trim();
                            var bestData = betterData.split(',');
                            total++;
                            checkVacancy(bestData);
                        }
                    }
                });
            }
        });
    };

    var checkVacancy = function(courtData) {
        var options = $("#exclusions option");
        var _skip = false;
        $.each(options, function(index, option) {
            console.log(option.text + "-" + courtData[4]);
            if(option.text == courtData[4]) {
                _skip = true;
                return;
            }
        });
        if(_skip) {
            count++;
            return;
        }

        var date = $('#date').val();
        var viewMode = $("#viewMode").val();
        var postDataObject = {
            searchDate: date,
            searchResource: 9,
            indSearchBy: 1,
            pageNo: 1,
            "btnCheckVacancy.x": 38,
            "btnCheckVacancy.y": 13,
            cdNationality: "SG",
            identificationType: "001",
            viewMode: viewMode,
            searchResultPerPage: 500,
            idProduct: courtData[0],
            idProfile: courtData[1],
            indBookingBasis: courtData[2],
            resourceDesc: courtData[3],
            entityName: courtData[4],
            txVenue: courtData[4],
            indPayCounter: courtData[5],
            indPublishPublic: courtData[6],
            idInternalBu: courtData[7],
            unitNum: courtData[8],
            cdResourceType: courtData[9],
            indIndemnityReq: courtData[10],
            cdNationality:'SG',
            identificationType:'001',
            indBookingBasis: 1,
            task: "",
            CWT: "",
            CWT: "",
            searchLocation: courtData[7]
        };

        var postData = objectToQueryParams(postDataObject);

        var query = encodeURIComponent("select * from htmlpost where url='https://one.pa.gov.sg/CRMSPortal/CRMSPortal.portal?_nfpb=true&_st=" +
            "&_windowLabel=CRMSPortal_1&_urlType=action&wlpCRMSPortal_1_action=RBMFacilityPublicBooking" +
            "&_pageLabel=CRMSPortal_page_1' and postdata=\"" + postData + "\"");
        var xpath = " and (xpath='//table[@class=\"main_table\"]/tbody/tr')";

        var otherParams = "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

        var url = YQL_ENPOINT + query + encodeURIComponent(xpath)  + otherParams;

        $.ajax({
            url: url
        })
            .done(function(data) {
                count++;
                $("#jqxProgressBar").progressbar({
                    value: 100 * count / total
                });
                status("Looking for vacancies at: " + courtData[4] + " - Court: " + courtData[8]);
                var trs = (data.query.results && data.query.results.postresult) ? extractVacancies(data.query.results.postresult.tr) : [];
                var vacancies = [];
                var timeIn24Hrs = to24hrs(time);
                if(trs) {
                    $.each(trs, function(index, tr) {
                        var result = null;
                        $.each(tr.td, function(index, td) {
                            if(td && td.content && vacancies.indexOf(td.content) < 0 && td.bgcolor == '#ffe5e5') {
                                if(tp == -1 && time != -1) {
                                    if(to24hrs(td.content.substring(0, 8)) <= timeIn24Hrs) {
                                        result = result ? td.content : result + ', ' + td.content;
                                    }
                                }
                                else if(tp == 1 && time != -1) {
                                    if(to24hrs(td.content.substring(0, 8)) >= timeIn24Hrs) {
                                        result = result ? td.content : result + ', ' + td.content;
                                    }
                                }
                                else {
                                    result = result ? td.content : result + ', ' + td.content;
                                }
                                if(result) {
                                    vacancies.push(td.content);
                                }
                            }
                        })
                    });
                }
                if(vacancies.length > 0) {
                    var formHtml = '<tr> <td><a target="_blank" href="http://gothere.sg/maps#q:' + encodeURIComponent(courtData[4])
                        + '">' + courtData[4] + '(' + vacancies.length  +')</a></td><td>'  + courtData[8]
                        + '</td><td>' + vacancies + '</td>';

                    formHtml = formHtml.concat('<td><form action="https://one.pa.gov.sg/CRMSPortal/CRMSPortal.portal?_nfpb=true' +
                        '&_st=&_windowLabel=CRMSPortal_1&_urlType=action&wlpCRMSPortal_1_action=RBMFacilityPublicBooking' +
                        '&_pageLabel=CRMSPortal_page_1" method="POST">');
                    for (var key in postDataObject) {
                        if (postDataObject.hasOwnProperty(key)) {
                            formHtml = formHtml.concat('<input type="hidden" value="' + postDataObject[key] + '" name="' + key + '" />');
                        }
                    }
                    formHtml = formHtml.concat("<button type='submit'>Book</button>");
                    formHtml = formHtml.concat("</form>");
                    formHtml = formHtml.concat("</td></tr>");
                    $("#result").append(formHtml);
                }
                if(count == total) {
                    status('Ready!');
                }
            })
            .fail(function(e) {
                count++;
                if(count == total) {
                    status('Ready!');
                }
            });
    }

    var extractVacancies = function(trs) {
        return $.grep(trs, function(tr, index) {
            var tds = tr.td;
            var hasVacancy = false;
            if(tds) {
                $.each(tds, function(index, td) {
                    if(td && td.input && td.input.id != 'NRIC') {
                        return hasVacancy = true;
                    }
                });
            }
            return hasVacancy;
        });
    }

    var objectToQueryParams = function(object) {
        var postData = "";

        for (var key in object) {
            if (object.hasOwnProperty(key)) {
                postData = postData.concat(key, "=", $.trim(object[key]), "&")
            }
        }
        postData = postData.substr(0, postData.lastIndexOf("&"));
        return postData;
    }

    var status = function(_status) {
        $("#status").html(_status);
    }

    function to24hrs(time) {
        var hours = time.substr(0, 2);
        if(time.indexOf('AM') != -1 && hours == 12) {
            time = time.replace('12', '0');
        }
        if(time.indexOf('PM')  != -1 && hours < 12) {
            time = time.replace(hours, (parseInt(hours) + 12));
        }
        return time.replace(/(AM|PM)/, '');
    }

    var searchBtn = $('#searchBtn');
    searchBtn.click(search);
    fulfillLocations();

    var locations = $("#locations");
    var exclusions = $("#exclusions");
    locations.dblclick(function(data) {
        $("#locations option[value='" + data.target.value + "']").remove();
        exclusions.prepend('<option value="' + data.target.value + '">' + data.target.label + '</option>');
    });

    exclusions.dblclick(function(data) {
        $("#exclusions option[value='" + data.target.value + "']").remove();
        locations.prepend('<option value="' + data.target.value + '">' + data.target.label + '</option>');
    });

});